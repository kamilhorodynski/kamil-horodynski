﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlashlightApp
{
    class Lightbulb
    {
        private int brightness = 0;

        public Lightbulb(int brightness)
        {
            this.brightness = brightness;
        }

        public int getLightbulbBrightness()
        {
            return this.brightness;
        }
    }
}
