﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlashlightApp
{
    class ControlPanel
    {
        private static void TurnFlashlightOn()
        {
            Flashlight.TurnedOn();
        }

        private static void TurnFlashlightOff()
        {
            Flashlight.TurnedOff();
        }

        private static void PutNewBatteryIn()
        {
            Flashlight.PutNewBatteryIn();
        }


        private static void takeBatteryOut()
        {
            Flashlight.takeBatteryOut();
        }

        private static void putNewLightbulbIn(int number)
        {
            Flashlight.putNewLightbulbIn(number);
        }

        private static void changeLightbulb(int number)
        {
            Flashlight.changeLightbulb(number);
        }

        public static void showSwitchMenu()
        {
            ConsoleGraphics.drawSwitchMenu();
            string s;
            int n;

            do
            {
                s = Console.ReadLine();
                n = int.Parse(s);
            } while (n < 1 || n > 6);

            switch (n)
            {
                case 1:
                    if (Flashlight.getPoweredOn() == false)
                    {
                        if (Flashlight.canBeTurnedOn() == true)
                        {
                            Console.Clear();
                            ControlPanel.TurnFlashlightOn();
                            ConsoleGraphics.drawGraphics(Flashlight.getBatteryCount(), Flashlight.getIsLightbulbInside(), Flashlight.getPoweredOn());
                        }
                        else
                        {
                            Console.Clear();
                            ConsoleGraphics.drawGraphics(Flashlight.getBatteryCount(), Flashlight.getIsLightbulbInside(), Flashlight.getPoweredOn());
                            Console.WriteLine("Cannot turn flashlight on. There must be min. 2 batteries with min. 15 charges and a lightbulb inside.");
                        }
                    }
                    else
                    {
                        if (Flashlight.canBeTurnedOn() == true)
                        {
                            Console.Clear();
                            ConsoleGraphics.drawGraphics(Flashlight.getBatteryCount(), Flashlight.getIsLightbulbInside(), Flashlight.getPoweredOn());
                            Console.WriteLine("Cannot turn flashlight on. Flashlight is already turned on.");
                        }
                        else
                        {
                            Console.Clear();
                            ConsoleGraphics.drawGraphics(Flashlight.getBatteryCount(), Flashlight.getIsLightbulbInside(), Flashlight.getPoweredOn());
                            Console.WriteLine("Cannot turn flashlight on. There must be min. 2 batteries with min. 15 charges and a lightbulb inside.");
                        }
                    }
                    showSwitchMenu();
                    break;
                case 2:
                    if (Flashlight.getPoweredOn() == true)
                    {
                        Console.Clear();
                        ControlPanel.TurnFlashlightOff();
                        ConsoleGraphics.drawGraphics(Flashlight.getBatteryCount(), Flashlight.getIsLightbulbInside(), Flashlight.getPoweredOn());
                    }
                    else
                    {
                        Console.Clear();
                        ConsoleGraphics.drawGraphics(Flashlight.getBatteryCount(), Flashlight.getIsLightbulbInside(), Flashlight.getPoweredOn());
                        Console.WriteLine("Cannot turn flashlight off. Flashlight is already turned off.");
                    }
                    showSwitchMenu();
                    break;
                case 3:
                    if (Flashlight.getBatteryCount() <= 3)
                    {
                        Console.Clear();
                        ControlPanel.PutNewBatteryIn();
                        ConsoleGraphics.drawGraphics(Flashlight.getBatteryCount(), Flashlight.getIsLightbulbInside(), Flashlight.getPoweredOn());
                    }
                    else
                    {
                        Console.Clear();
                        ConsoleGraphics.drawGraphics(Flashlight.getBatteryCount(), Flashlight.getIsLightbulbInside(), Flashlight.getPoweredOn());
                        Console.WriteLine("Cannot put more batteries in. There are maximum of 4 batteries already inside.");
                    }
                    showSwitchMenu();
                    break;
                case 4:
                    if (Flashlight.getPoweredOn() == false)
                    {
                        if(Flashlight.getBatteryCount() > 0)
                        {
                            Console.Clear();
                            ControlPanel.takeBatteryOut();
                            ConsoleGraphics.drawGraphics(Flashlight.getBatteryCount(), Flashlight.getIsLightbulbInside(), Flashlight.getPoweredOn());
                        } else
                        {
                            Console.Clear();
                            ConsoleGraphics.drawGraphics(Flashlight.getBatteryCount(), Flashlight.getIsLightbulbInside(), Flashlight.getPoweredOn());
                            Console.WriteLine("Cannot take battery out. There is no battery inside.");
                        }
                    } else
                    {
                        if (Flashlight.getBatteryCount() > 0)
                        {
                            Console.Clear();
                            ConsoleGraphics.drawGraphics(Flashlight.getBatteryCount(), Flashlight.getIsLightbulbInside(), Flashlight.getPoweredOn());
                            Console.WriteLine("Cannot take battery out. The flashlight must be turned off for this action to perform.");
                        }
                        else
                        {
                            Console.Clear();
                            ConsoleGraphics.drawGraphics(Flashlight.getBatteryCount(), Flashlight.getIsLightbulbInside(), Flashlight.getPoweredOn());
                            Console.WriteLine("Cannot take battery out. There is no battery inside.");
                        }
                    }
                    showSwitchMenu();
                    break;
                case 5:
                    int power;
                    string powerString;
                    if (Flashlight.getIsLightbulbInside() == false)
                    {
                        do
                        {
                            Console.WriteLine("Enter lightbulb brightness (1-10): ");
                            powerString = Console.ReadLine();
                            power = int.Parse(powerString);
                        } while (power < 1 || power > 10);
                        Console.Clear();
                        ControlPanel.putNewLightbulbIn(power);
                        ConsoleGraphics.drawGraphics(Flashlight.getBatteryCount(), Flashlight.getIsLightbulbInside(), Flashlight.getPoweredOn());
                    }
                    else
                    {
                        Console.Clear();
                        ConsoleGraphics.drawGraphics(Flashlight.getBatteryCount(), Flashlight.getIsLightbulbInside(), Flashlight.getPoweredOn());
                        Console.WriteLine("Cannot put another lightbulb in. There is already one inside. Try change lightbulb function.");
                    }
                    showSwitchMenu();
                    break;
                case 6:
                    if (Flashlight.getPoweredOn() == false && Flashlight.getIsLightbulbInside() == true)
                    {
                        Console.WriteLine("Enter lightbulb brightness (1-10): ");
                        powerString = Console.ReadLine();
                        power = int.Parse(powerString);
                        Console.Clear();
                        ControlPanel.changeLightbulb(power);
                        ConsoleGraphics.drawGraphics(Flashlight.getBatteryCount(), Flashlight.getIsLightbulbInside(), Flashlight.getPoweredOn());
                    }
                    else
                    {
                        Console.Clear();
                        ConsoleGraphics.drawGraphics(Flashlight.getBatteryCount(), Flashlight.getIsLightbulbInside(), Flashlight.getPoweredOn());
                        Console.WriteLine("Cannot change the lightbulb. There must be lightbulb inside and flashlight must be turned off for this action to be performed.");
                    }
                    showSwitchMenu();
                    break;
                default:
                    Console.WriteLine("Default case");
                    break;
            }
        }
    }
}
