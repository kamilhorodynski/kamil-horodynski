﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlashlightApp
{
    class Flashlight
    {
        private static IList<Battery> batteries = new List<Battery>();
        private static IList<Lightbulb> lightbulbs = new List<Lightbulb>();
        private static bool poweredOn = false;

        public static int getSpecificBatteryPower(int batteryNumber)
        {
            int power = Flashlight.batteries[batteryNumber].getBatteryPower();
            return power;
        }

        public static int getSpecificLightbulbBrightness(int lightbulbNumber)
        {
            int brightness = Flashlight.lightbulbs[lightbulbNumber].getLightbulbBrightness();
            return brightness;
        }

        public static void PutNewBatteryIn()
        {
            batteries.Add(new Battery());
        }

        public static void takeBatteryOut()
        {
            batteries.RemoveAt(getBatteryCount() - 1);
        }

        public static void putNewLightbulbIn(int number)
        {
            lightbulbs.Add(new Lightbulb(number));
        }

        public static void changeLightbulb(int number)
        {
            lightbulbs.RemoveAt(getLightbulbsCount() - 1);
            lightbulbs.Add(new Lightbulb(number));
        }


        public static bool canBeTurnedOn()
        {
            if (getNotDepleatedBatteriesCount() >= 2 && getLightbulbsCount() >= 1)
            {
                return true;
            }
            else return false;
        }

        public static void TurnedOn()
        {
            if (canBeTurnedOn() == true)
            {
                poweredOn = true;
                UseCharge();
            }

        }

        public static void TurnedOff()
        {
            {
                poweredOn = false;
            }
        }

        public static bool getPoweredOn()
        {
            return poweredOn;
        }

        public static int getNotDepleatedBatteriesCount()
        {
            int count = 0;

            for (int i = 1; i <= batteries.Count; i++)
            {
                if (batteries[i - 1].getBatteryPower() >= 15)
                {
                    count++;
                }
            }

            return count;
        }

        public static int getBatteryCount()
        {
            return batteries.Count;
        }

        public static void UseCharge()
        {
            for (int i = 1; i <= Flashlight.getBatteryCount(); i++)
            {
                batteries[i - 1].drainBattery(lightbulbs[0].getLightbulbBrightness());
            }
        }

        public static int getLightbulbsCount()
        {
            return lightbulbs.Count;
        }

        public static bool getIsLightbulbInside()
        {
            if (getLightbulbsCount() >= 1)
            {
                return true;
            }
            else return false;
        }
    }
}
