﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlashlightApp
{
    class ConsoleGraphics
    {
        public static void drawSwitchMenu()
        {
            Console.WriteLine("1. Turn flashlight on");
            Console.WriteLine("2. Turn flashlight off");
            Console.WriteLine("3. Put new battery in");
            Console.WriteLine("4. Take battery out");
            Console.WriteLine("5. Put lightbulb in");
            Console.WriteLine("6. Change lightbulb");
            Console.WriteLine();
        }

        public static void drawGraphics(int batteryCount, bool LightbulbInside, bool poweredOn)
        {
            if (LightbulbInside == true)
            {
                if (batteryCount == 0)
                {
                    if (poweredOn == true)
                    {
                    }
                    else
                    {
                        drawFlashlightTurnedOffWithLightbulb();
                    }
                }
                if (batteryCount == 1)
                {
                    if (poweredOn == true)
                    {
                    }
                    else
                    {
                        drawFlashlightTurnedOffWithLightbulbAnd1Battery();
                    }
                }
                if (batteryCount == 2)
                {
                    if (poweredOn == true)
                    {
                        drawFlashlightTurnedOnWith2Batteries();
                    }
                    else
                    {
                        drawFlashlightTurnedOffWithLightbulbAnd2Batteries();
                    }
                }
                if (batteryCount == 3)
                {
                    if (poweredOn == true)
                    {
                        drawFlashlightTurnedOnWith3Batteries();
                    }
                    else
                    {
                        drawFlashlightTurnedOffWithLightbulbAnd3Batteries();
                    }
                }
                if (batteryCount == 4)
                {
                    if (poweredOn == true)
                    {
                        drawFlashlightTurnedOnWith4Batteries();
                    }
                    else
                    {
                        drawFlashlightTurnedOffWithLightbulbAnd4Batteries();
                    }
                }
            }
            else
            {
                if (batteryCount == 0)
                {
                    drawFlashlightTurnedOffWithoutLightbulbAndWithoutBatteries();
                }
                if (batteryCount == 1)
                {
                    drawFlashlightTurnedOffWithoutLightbulbAndWith1Battery();
                }
                if (batteryCount == 2)
                {
                    drawFlashlightTurnedOffWithoutLightbulbAndWith2Batteries();
                }
                if (batteryCount == 3)
                {
                    drawFlashlightTurnedOffWithoutLightbulbAndWith3Batteries();
                }
                if (batteryCount == 4)
                {
                    drawFlashlightTurnedOffWithoutLightbulbAndWith4Batteries();
                }
            }


            drawStatistics();
        }

        private static void ShowBatteryPower(int batteryNumber)
        {
            if (Flashlight.getBatteryCount() > 0)
            {
                Console.WriteLine("Battery number " + (batteryNumber + 1) + " have " + Flashlight.getSpecificBatteryPower(batteryNumber) + " charges left");
            }
        }

        private static void ShowLightbulbBrightness()
        {
            Console.WriteLine("Installed lightbulb have brightness: " + Flashlight.getSpecificLightbulbBrightness(0));
        }

        private static void drawStatistics()
        {
            Console.WriteLine();
            for (int i = 1; i <= Flashlight.getBatteryCount(); i++)
            {
                ShowBatteryPower(i - 1);
            }
            for (int i = 1; i <= Flashlight.getLightbulbsCount(); i++)
            {
                ShowLightbulbBrightness();
            }

            Console.WriteLine("There is " + Flashlight.getBatteryCount() + " batteries installed and " + Flashlight.getLightbulbsCount() + " lightbulb.");
        }

        private static void drawFlashlightTurnedOnWith4Batteries()
        {
            Console.WriteLine("                 ___________");
            Console.WriteLine("****************|           |");
            Console.WriteLine("****************|           |___________________________________________________");
            Console.WriteLine("****************|       ----                                                    |");
            Console.WriteLine("****************|      ( {} ) [==========][==========][==========][==========]  |");
            Console.WriteLine("****************|       ---- ___________________________________________________|");
            Console.WriteLine("****************|           |");
            Console.WriteLine("****************|___________|");
            Console.WriteLine();
        }

        private static void drawFlashlightTurnedOnWith3Batteries()
        {
            Console.WriteLine("                 ___________");
            Console.WriteLine("****************|           |");
            Console.WriteLine("****************|           |___________________________________________________");
            Console.WriteLine("****************|       ----                                                    |");
            Console.WriteLine("****************|      ( {} ) [==========][==========][==========]              |");
            Console.WriteLine("****************|       ---- ___________________________________________________|");
            Console.WriteLine("****************|           |");
            Console.WriteLine("****************|___________|");
            Console.WriteLine();
        }

        private static void drawFlashlightTurnedOnWith2Batteries()
        {
            Console.WriteLine("                 ___________");
            Console.WriteLine("****************|           |");
            Console.WriteLine("****************|           |___________________________________________________");
            Console.WriteLine("****************|       ----                                                    |");
            Console.WriteLine("****************|      ( {} ) [==========][==========]                          |");
            Console.WriteLine("****************|       ---- ___________________________________________________|");
            Console.WriteLine("****************|           |");
            Console.WriteLine("****************|___________|");
            Console.WriteLine();
        }

        private static void drawFlashlightTurnedOffWithLightbulbAnd4Batteries()
        {
            Console.WriteLine("                 ___________");
            Console.WriteLine("                |           |");
            Console.WriteLine("                |           |___________________________________________________");
            Console.WriteLine("                |       ----                                                    |");
            Console.WriteLine("                |      ( {} ) [==========][==========][==========][==========]  |");
            Console.WriteLine("                |       ---- ___________________________________________________|");
            Console.WriteLine("                |           |");
            Console.WriteLine("                |___________|");
            Console.WriteLine();
        }

        private static void drawFlashlightTurnedOffWithLightbulbAnd3Batteries()
        {
            Console.WriteLine("                 ___________");
            Console.WriteLine("                |           |");
            Console.WriteLine("                |           |___________________________________________________");
            Console.WriteLine("                |       ----                                                    |");
            Console.WriteLine("                |      ( {} ) [==========][==========][==========]              |");
            Console.WriteLine("                |       ---- ___________________________________________________|");
            Console.WriteLine("                |           |");
            Console.WriteLine("                |___________|");
            Console.WriteLine();
        }

        private static void drawFlashlightTurnedOffWithLightbulbAnd2Batteries()
        {
            Console.WriteLine("                 ___________");
            Console.WriteLine("                |           |");
            Console.WriteLine("                |           |___________________________________________________");
            Console.WriteLine("                |       ----                                                    |");
            Console.WriteLine("                |      ( {} ) [==========][==========]                          |");
            Console.WriteLine("                |       ---- ___________________________________________________|");
            Console.WriteLine("                |           |");
            Console.WriteLine("                |___________|");
            Console.WriteLine();
        }

        private static void drawFlashlightTurnedOffWithLightbulbAnd1Battery()
        {
            Console.WriteLine("                 ___________");
            Console.WriteLine("                |           |");
            Console.WriteLine("                |           |___________________________________________________");
            Console.WriteLine("                |       ----                                                    |");
            Console.WriteLine("                |      ( {} ) [==========]                                      |");
            Console.WriteLine("                |       ---- ___________________________________________________|");
            Console.WriteLine("                |           |");
            Console.WriteLine("                |___________|");
            Console.WriteLine();
        }

        private static void drawFlashlightTurnedOffWithLightbulb()
        {
            Console.WriteLine("                 ___________");
            Console.WriteLine("                |           |");
            Console.WriteLine("                |           |___________________________________________________");
            Console.WriteLine("                |       ----                                                    |");
            Console.WriteLine("                |      ( {} )                                                   |");
            Console.WriteLine("                |       ---- ___________________________________________________|");
            Console.WriteLine("                |           |");
            Console.WriteLine("                |___________|");
            Console.WriteLine();
        }

        private static void drawFlashlightTurnedOffWithoutLightbulbAndWith4Batteries()
        {
            Console.WriteLine("                 ___________");
            Console.WriteLine("                |           |");
            Console.WriteLine("                |           |___________________________________________________");
            Console.WriteLine("                |                                                               |");
            Console.WriteLine("                |             [==========][==========][==========][==========]  |");
            Console.WriteLine("                |            ___________________________________________________|");
            Console.WriteLine("                |           |");
            Console.WriteLine("                |___________|");
            Console.WriteLine();
        }

        private static void drawFlashlightTurnedOffWithoutLightbulbAndWith3Batteries()
        {
            Console.WriteLine("                 ___________");
            Console.WriteLine("                |           |");
            Console.WriteLine("                |           |___________________________________________________");
            Console.WriteLine("                |                                                               |");
            Console.WriteLine("                |             [==========][==========][==========]              |");
            Console.WriteLine("                |            ___________________________________________________|");
            Console.WriteLine("                |           |");
            Console.WriteLine("                |___________|");
            Console.WriteLine();
        }

        private static void drawFlashlightTurnedOffWithoutLightbulbAndWith2Batteries()
        {
            Console.WriteLine("                 ___________");
            Console.WriteLine("                |           |");
            Console.WriteLine("                |           |___________________________________________________");
            Console.WriteLine("                |                                                               |");
            Console.WriteLine("                |             [==========][==========]                          |");
            Console.WriteLine("                |            ___________________________________________________|");
            Console.WriteLine("                |           |");
            Console.WriteLine("                |___________|");
            Console.WriteLine();
        }

        private static void drawFlashlightTurnedOffWithoutLightbulbAndWith1Battery()
        {
            Console.WriteLine("                 ___________");
            Console.WriteLine("                |           |");
            Console.WriteLine("                |           |___________________________________________________");
            Console.WriteLine("                |                                                               |");
            Console.WriteLine("                |             [==========]                                      |");
            Console.WriteLine("                |            ___________________________________________________|");
            Console.WriteLine("                |           |");
            Console.WriteLine("                |___________|");
            Console.WriteLine();
        }

        private static void drawFlashlightTurnedOffWithoutLightbulbAndWithoutBatteries()
        {
            Console.WriteLine("                 ___________");
            Console.WriteLine("                |           |");
            Console.WriteLine("                |           |___________________________________________________");
            Console.WriteLine("                |                                                               |");
            Console.WriteLine("                |                                                               |");
            Console.WriteLine("                |            ___________________________________________________|");
            Console.WriteLine("                |           |");
            Console.WriteLine("                |___________|");
            Console.WriteLine();
        }


    }
}
