﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlashlightApp
{
    class Battery
    {
        private int power = 100;

        public void setBatteryPower(int batPower)
        {
            this.power = batPower;
        }

        public void drainBattery(int drainValue)
        {
            this.power = this.power - drainValue;
            if (this.power < 0)
            {
                this.power = 0;
            }
        }

        public int getBatteryPower()
        {
            return this.power;
        }
    }
}
