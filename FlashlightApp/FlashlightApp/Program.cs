﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlashlightApp
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleGraphics.drawGraphics(Flashlight.getBatteryCount(), Flashlight.getIsLightbulbInside(), Flashlight.getPoweredOn());
            ControlPanel.showSwitchMenu();
            Console.ReadKey();
        }
    }
}
